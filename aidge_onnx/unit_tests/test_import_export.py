import unittest
import aidge_onnx

import requests

def download_onnx_model(url, destination):
    try:
        response = requests.get(url, stream=True)
        response.raise_for_status()

        with open(destination, 'wb') as file:
            for chunk in response.iter_content(chunk_size=8192):
                file.write(chunk)

        print(f"Downloaded ONNX model to {destination}")
    except Exception as e:
        print(f"Error downloading ONNX model: {e}")

class test_load_save(unittest.TestCase):

    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_converter(self):
        url = "https://github.com/onnx/models/raw/69d69010b7ed6ba9438c392943d2715026792d40/Computer_Vision/mobilenetv2_050_Opset18_timm/mobilenetv2_050_Opset18.onnx?download="
        og_filename = "mobilenetV2.onnx"
        aidge_filename = "aidge_mobilenetV2.onnx"
        download_onnx_model(url, og_filename)
        model = aidge_onnx.load_onnx(og_filename, verbose=True)
        assert len(model.get_input_nodes()) == 1
        assert len(model.get_output_nodes()) == 1
        aidge_onnx.export_onnx(
            model,
            aidge_filename,
            inputs_dims={
                list(model.get_input_nodes())[0].name():[[1,3,224,224]]
            },
            outputs_dims={
                list(model.get_output_nodes())[0].name():[[1,1000]]
            },
            opset=18
        )
        self.assertTrue(aidge_onnx.check_onnx_validity(aidge_filename))

if __name__ == '__main__':
    unittest.main()
