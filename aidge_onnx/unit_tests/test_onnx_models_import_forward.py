"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import numpy as np
import pytest
import re
import os

import onnx
import onnxruntime

import aidge_core
import aidge_backend_cpu  # noqa: F401
import aidge_onnx

from utils.onnx_helpers import onnx_generate_input_data
from utils.pytest_helpers import PytestParamsGenerator

# This test is linked to test_onnx_nodels_import.py test which does the import
# XFAIL/SKIP lists for import are added to this test XFAIL/SKIP lists
from test_onnx_models_import import (
    TEST_IMPORT_ONNX_VERBOSE,
    ONNX_TESTS,
    op_ident_func,
    XFAIL_ONNX_IMPORT_MODELS,
    SKIP_ONNX_IMPORT_MODELS,
)


# As of now, aidge forward is very slow on the imported models,
# default is to skip unless TEST_ONNX_ALL is set to 1
TEST_ONNX_ALL = bool(int(os.getenv("TEST_ONNX_ALL", "0")))

# XFAIL list for test_onnx_import_models_forward(),
# added to the XFAIL_ONNX_IMPORT_MODELS list
# Attempt to keep oneliners (<= 88 cols)
XFAIL_ONNX_IMPORT_MODELS_FORWARD = [
    (re.compile("densenet121"), "conv shape mismatch"),
    (re.compile("light_inception_v2"), "concat input shapes mismatch"),
    (re.compile("shufflenet"), "convdepthwise shape mismatch"),
    (re.compile("squeezenet"), "output shape mismatch"),
]

# SKIP list for test_onnx_import_nodes_forward(),
# added to the SKIP_ONNX_IMPORT_NODES list
# Note that there may be tests which segfault or run into infinite loops,
# they should be put there instead of the XFAIL list
# Note that these tests will not be reported as XPASS when passing,
# comment them to verify
# Attempt to keep oneliners (<= 88 cols)
SKIP_ONNX_IMPORT_MODELS_FORWARD = []

if not TEST_ONNX_ALL:
    SKIP_ONNX_IMPORT_MODELS_FORWARD = [(re.compile(""), "skip all models execution")]


def generate_test_onnx_import_models_forward():
    all_tests = [
        (model, dataset)
        for model in ONNX_TESTS.get_models()
        for dataset in ONNX_TESTS.get_model_datasets(model)
    ]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=op_ident_func,
        skips=SKIP_ONNX_IMPORT_MODELS + SKIP_ONNX_IMPORT_MODELS_FORWARD,
        xfails=XFAIL_ONNX_IMPORT_MODELS + XFAIL_ONNX_IMPORT_MODELS_FORWARD,
    ).generate()
    return params


@pytest.mark.parametrize(
    "onnx_model_path, onnx_dataset_path", generate_test_onnx_import_models_forward()
)
def test_onnx_import_models_forward(onnx_model_path, onnx_dataset_path, tmpdir):
    model_path = ONNX_TESTS.test_path / onnx_model_path
    onnx_model = onnx.load(str(model_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_model_path}:\n{onnx_model}")
    onnx_inputs, onnx_outputs = ONNX_TESTS.onnx_load_dataset(onnx_dataset_path)
    onnx_session = onnxruntime.InferenceSession(model_path)
    onnx_initializers = {ini.name: ini for ini in onnx_model.graph.initializer}
    np_inputs_map = {
        inp.name: (
            onnx.numpy_helper.to_array(onnx_inputs[inp.name])
            if inp.name in onnx_inputs
            else onnx_generate_input_data(inp)
        )
        for inp in onnx_model.graph.input
        if inp.name not in onnx_initializers
    }
    outputs_names = [out.name for out in onnx_model.graph.output]
    onnx_outputs = onnx_session.run(outputs_names, np_inputs_map)

    if TEST_IMPORT_ONNX_VERBOSE:
        aidge_core.Log.set_console_level(aidge_core.Level.Debug)
    model = aidge_onnx.load_onnx(str(model_path), verbose=TEST_IMPORT_ONNX_VERBOSE)
    model.set_backend("cpu")
    scheduler = aidge_core.SequentialScheduler(model)

    aidge_inputs_list = [aidge_core.Tensor(v) for v in np_inputs_map.values()]

    scheduler.forward(forward_dims=False, data=aidge_inputs_list)
    aidge_outputs = [
        np.array(out[0].get_operator().get_output(out[1]))
        for out in model.get_ordered_outputs()
    ]

    for idx, (onnx_out, aidge_out) in enumerate(zip(onnx_outputs, aidge_outputs)):
        assert (
            onnx_out.dtype == aidge_out.dtype
        ), f"output #{idx} dtypes do not match: {onnx_out.dtype} != {aidge_out.dtype}"
        assert (
            onnx_out.shape == aidge_out.shape
        ), f"output #{idx} shapes do not match: {onnx_out.shape} != {aidge_out.shape}"
        assert np.allclose(
            onnx_out, aidge_out
        ), f"output #{idx} do not match: {onnx_out} != {aidge_out}"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
