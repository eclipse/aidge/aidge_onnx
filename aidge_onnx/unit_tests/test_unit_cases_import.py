"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

from pathlib import Path
import re
import os
import pytest

import onnx

import aidge_onnx

from utils.pytest_helpers import PytestParamsGenerator
from utils.onnx_helpers import onnx_from_test
from utils.aidge_helpers import model_convert_aidge_nodes, model_get_nodes_op_class

# Set to 1 for dumping onnx and verbose import on aidge_onnx.load_onnx()
TEST_IMPORT_ONNX_VERBOSE = bool(int(os.getenv("TEST_IMPORT_ONNX_VERBOSE", "0")))

TEST_IDENTITY = {
    "name": "identity",
    "inputs": [
        ("x", "float", ()),
    ],
    "outputs": [
        ("x", "float", ()),
    ],
    "nodes": [],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
            ],
            "outputs": [
                (3, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_nodes": 1,  # x -id-> x
    },
}

TEST_SWAP = {
    "name": "swap",
    "inputs": [
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("y", "float", ()),
        ("x", "float", ()),
    ],
    "nodes": [],
    "datasets": [
        {
            "inputs": [
                (1, "float32"),
                (3, "float32"),
            ],
            "outputs": [
                (3, "float32"),
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_nodes": 2,  # x -id-> y; y -id-> x
    },
}

TEST_SUB = {
    "name": "sub",
    "inputs": [
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
    },
}

TEST_SUB_x_x = {
    "name": "sub_x_x",
    "inputs": [
        ("x", "float", ()),
    ],
    "outputs": [
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "x"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
            ],
            "outputs": [
                (0, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_nodes": 2,  # x -id-> t ; t, t -sub-> z
    },
}

TEST_SUB_y_x = {
    "name": "sub_y_x",
    "inputs": [
        ("y", "float", ()),
        ("x", "float", ()),
    ],
    "outputs": [
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                (-1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
    },
}

TEST_IGNORED_INPUT = {
    "name": "ignored_input",
    "inputs": [
        ("", "float", ()),
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                None,
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
        "nb_inputs": 2,  # conversion ignores the first input
    },
}

TEST_UNUSED_INPUT = {
    "name": "unused_input",
    "inputs": [
        ("x", "float", ()),
        ("unused", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                None,
                (2, "float32"),
            ],
            "outputs": [
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
        "nb_inputs": 2,  # conversion ignores the second input
    },
}

TEST_IGNORED_OUTPUT = {
    "name": "ignored_output",
    "inputs": [
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("", "float", ()),
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                None,
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
        "nb_outputs": 1,  # conversion ignores the first output
        "nb_output_nodes": 1,  # conversion ignores the first output
    },
}

TEST_IGNORED_OUTPUT_AND_UNUSED = {
    "name": "ignored_output_and_unused",
    "inputs": [
        ("inp", "float", (1, 4)),
    ],
    "outputs": [
        ("", "float", (1, 2)),
        ("out2", "float", (1, 2)),
    ],
    "initializers": [
        ("split", "int64", (2,), [2, 2]),
    ],
    "nodes": [
        {
            "op": "Split",
            "inputs": ["inp", "split"],
            "outputs": ["out1", "out2"],
            "attributes": {
                "axis": 1,
            },
        },
    ],
    "datasets": [
        {
            "inputs": [
                ([[1, 2, 3, 4]], "float32"),
            ],
            "outputs": [
                None,
                ([[3, 4]], "float32"),
                ([[1, 2]], "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_output_nodes": 1,  # split
        "nb_outputs": 2,  # ignores the first output, but adds the unused split output
    },
}


TEST_UNDEF_OUTPUT = {
    "name": "undef_output",
    "inputs": [
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("undef", "float", ()),
        ("z", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                None,
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
        "nb_outputs": 1,  # conversion ignores the first output
        "nb_output_nodes": 1,  # conversion ignores the first output
    },
}

TEST_UNDEF_OUTPUT_AND_UNUSED = {
    "name": "undef_output_and_unused",
    "inputs": [
        ("x", "float", ()),
        ("y", "float", ()),
    ],
    "outputs": [
        ("undef", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                None,
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_input_nodes": 1,  # sub
        "nb_outputs": 1,  # ignores the first output, but adds the unused sub output
    },
}

TEST_UNDEF_AND_UNUSED_OP = {
    "name": "undef_and_unused_op",
    "inputs": [],
    "outputs": [],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["z"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                (1, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_inputs": 2,  # conversion adds exposed uses to inputs
        "nb_input_nodes": 1,  # sub
        "nb_outputs": 1,  # conversion adds unused defs to outputs
        "nb_output_nodes": 1,  # sub
    },
}

TEST_LOOP_NOP = {
    "name": "loop_nop",
    "inputs": [
        ("init", "float", ()),
    ],
    "outputs": [
        ("out", "float", ()),
    ],
    "nodes": [
        {
            "op": "aidge::Memorize",
            "inputs": ["back", "init"],
            "outputs": ["out", "back"],
            "attributes": {
                "end_step": 1,
            },
        },
    ],
    "datasets": [
        {
            "inputs": [
                (3, "float32"),
            ],
            "outputs": [
                (3, "float32"),
            ],
        },
    ],
}

TEST_LOOP_INC = {
    "name": "loop_inc",
    "inputs": [
        ("init", "float", ()),
        ("incr", "float", ()),
    ],
    "outputs": [
        ("out", "float", ()),
    ],
    "nodes": [
        {
            "op": "aidge::Memorize",
            "inputs": ["back", "init"],
            "outputs": ["out", "iter"],
            "attributes": {
                "end_step": 10,
            },
        },
        {
            "op": "Add",
            "inputs": ["iter", "incr"],
            "outputs": ["back"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (0, "float32"),
                (1, "float32"),
            ],
            "outputs": [
                (10, "float32"),  # init + end_step * incr
            ],
        },
    ],
}

TEST_SPLIT_ATTR = {
    "name": "split_attr",
    "inputs": [
        ("inp", "float", (1, 4)),
    ],
    "outputs": [
        ("out1", "float", (1, 2)),
        ("out2", "float", (1, 2)),
    ],
    "nodes": [
        {
            "op": "Split",
            "inputs": ["inp"],
            "outputs": ["out1", "out2"],
            "attributes": {
                "axis": 1,
                "num_outputs": 2,
            },
        },
    ],
    "datasets": [
        {
            "inputs": [
                ([[1, 2, 3, 4]], "float32"),
            ],
            "outputs": [
                ([[1, 2]], "float32"),
                ([[3, 4]], "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_inputs": 2,  # inp + optional input
        "nb_output_nodes": 1,  # split
    },
}

TEST_SPLIT_INPUT = {
    "name": "split_input",
    "inputs": [
        ("inp", "float", (1, 4)),
    ],
    "outputs": [
        ("out1", "float", (1, 2)),
        ("out2", "float", (1, 2)),
    ],
    "initializers": [
        ("split", "int64", (2,), [2, 2]),
    ],
    "nodes": [
        {
            "op": "Split",
            "inputs": ["inp", "split"],
            "outputs": ["out1", "out2"],
            "attributes": {
                "axis": 1,
            },
        },
    ],
    "datasets": [
        {
            "inputs": [
                ([[1, 2, 3, 4]], "float32"),
            ],
            "outputs": [
                ([[1, 2]], "float32"),
                ([[3, 4]], "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_output_nodes": 1,  # split
    },
}

TEST_NAME_REUSE = {
    "name": "name_reuse",
    "inputs": [
        ("inp1", "float", ()),
        ("inp2", "float", ()),
    ],
    "outputs": [
        ("out", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["inp1", "inp2"],
            "outputs": ["out"],
        },
        {
            "op": "Sub",
            "inputs": ["out", "inp2"],
            "outputs": ["out"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (7, "float32"),
                (2, "float32"),
            ],
            "outputs": [
                (3, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_nodes": 3,  # inp2 -identity-> 1, 2; inp1, 1 -sub-> out; out, 2 -sub-> out
    },
}

TEST_DROPOUT_INPUT = {
    "name": "dropout",
    "inputs": [
        ("x", "float", ()),
        ("r", "float", ()),
        ("t", "float", ()),
    ],
    "outputs": [
        ("y", "float", ()),
    ],
    "nodes": [
        {
            "op": "Dropout",
            "inputs": ["x", "r", "t"],
            "outputs": ["y"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (7, "float32"),
                (0.5, "float32"),
                (0, "float32"),
            ],
            "outputs": [
                (7, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_inputs": 1,  # x (r, t are ignored)
        "nb_input_nodes": 1,  # dropout
    },
}

TEST_DROPOUT_OPTIONAL = {
    "name": "dropout_optional",
    "inputs": [
        ("x", "float", ()),
        ("", "float", ()),
        ("t", "float", ()),
    ],
    "outputs": [
        ("y", "float", ()),
    ],
    "nodes": [
        {
            "op": "Dropout",
            "inputs": ["x", "", "t"],
            "outputs": ["y"],
        },
    ],
    "datasets": [
        {
            "inputs": [
                (7, "float32"),
                None,
                (0, "float32"),
            ],
            "outputs": [
                (7, "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_inputs": 1,  # x (others are ignored)
        "nb_input_nodes": 1,  # dropout
    },
}

TEST_LSTM = {
    "name": "lstm",
    "vars": {
        "input_size": 2,
        "hidden_size": 1,
        "seq_length": 1,
        "batch_size": 1,
        "num_directions": 1,
    },
    "initializers": [
        ("W", "float", ("num_directions", "4*hidden_size", "input_size"), 0.1),
    ],
    "inputs": [
        ("X", "float", ("seq_length", "batch_size", "input_size")),
        ("R", "float", ("num_directions", "4*hidden_size", "hidden_size")),
        ("B", "float", ("num_directions", "8*hidden_size")),
        ("sequence_lens", "float", ("batch_size",)),
        ("initial_h", "float", ("num_directions", "batch_size", "hidden_size")),
        ("initial_c", "float", ("num_directions", "batch_size", "hidden_size")),
        ("P", "float", ("num_directions", "3*hidden_size")),
    ],
    "outputs": [
        ("Y", "float", ("seq_length", "num_directions", "batch_size", "hidden_size")),
        ("Y_h", "float", ("num_directions", "batch_size", "hidden_size")),
        ("Y_c", "float", ("num_directions", "batch_size", "hidden_size")),
    ],
    "nodes": [
        {
            "op": "LSTM",
            "inputs": [
                "X",
                "W",
                "R",
                "B",
                "sequence_lens",
                "initial_h",
                "initial_c",
                "P",
            ],
            "outputs": ["Y", "Y_h", "Y_c"],
            "attributes": {
                "hidden_size": "hidden_size",
            },
        },
    ],
    # "datasets": [] # No execution as of now, just test import
    "import_check": {
        "nb_inputs": 77,  # apparently the number of inputs is 77, to be checked
        "nb_input_nodes": 1,  # lstm
        "nb_output_nodes": 1,  # lstm
    },
}

TEST_UNUSED_INITIALIZER = {
    "name": "unused_init",
    "initializers": [
        ("x", "float", (2,), [2, 2]),
        ("y", "float", (2,), [3, 3]),
        ("z", "float", (2,), [4, 4]),
        ("", "float", (2,), [5, 5]),
    ],

    "outputs": [
        ("o", "float", ()),
    ],
    "nodes": [
        {
            "op": "Sub",
            "inputs": ["x", "y"],
            "outputs": ["o"],
        },
    ],
    "datasets": [
        {
            "inputs": [],
            "outputs": [
                ([-1, -1], "float32"),
            ],
        },
    ],
    "import_check": {
        "nb_inputs": 0,  # no input
        "nb_output_nodes": 1,  # initializers "z" and "" are removed by default
        "nb_nodes": 3, # 2 producers + sub
    },
}

TEST_IMPORT_CASES = [
    TEST_IDENTITY,
    TEST_SWAP,
    TEST_SUB,
    TEST_SUB_x_x,
    TEST_SUB_y_x,
    TEST_IGNORED_INPUT,
    TEST_UNUSED_INPUT,
    TEST_IGNORED_OUTPUT,
    TEST_IGNORED_OUTPUT_AND_UNUSED,
    TEST_UNDEF_OUTPUT,
    TEST_UNDEF_OUTPUT_AND_UNUSED,
    TEST_UNDEF_AND_UNUSED_OP,
    TEST_LOOP_NOP,
    TEST_LOOP_INC,
    TEST_SPLIT_ATTR,
    TEST_SPLIT_INPUT,
    TEST_NAME_REUSE,
    TEST_DROPOUT_INPUT,
    TEST_DROPOUT_OPTIONAL,
    TEST_LSTM,
    TEST_UNUSED_INITIALIZER,
]

XFAIL_TEST_IMPORT = []

SKIP_TEST_IMPORT = [
    (re.compile("^loop_nop"), "import Memorize: segfault"),
]


def generate_test_import():
    all_tests = [(t["name"], t) for t in TEST_IMPORT_CASES]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=lambda name, test: name,
        skips=SKIP_TEST_IMPORT,
        xfails=XFAIL_TEST_IMPORT,
    ).generate()
    return params


@pytest.mark.parametrize("onnx_name, onnx_test", generate_test_import())
def test_import(onnx_name, onnx_test, tmpdir):
    onnx_path = Path(tmpdir) / f"{onnx_name}.onnx"
    onnx_model = onnx_from_test(onnx_test)
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_name}:\n{onnx_model}")
    onnx.save(onnx_model, str(onnx_path))
    model = aidge_onnx.load_onnx(str(onnx_path), verbose=TEST_IMPORT_ONNX_VERBOSE)
    model_convert_aidge_nodes(model)
    generics = model_get_nodes_op_class(model, "GenericOperatorOp")
    assert (
        len(generics) == 0
    ), f"GenericOperatorOp generated for nodes {[node.name() for node in generics]}"
    model_nodes = model.get_nodes()
    model_inputs = model.get_ordered_inputs()
    model_outputs = model.get_ordered_outputs()
    model_input_nodes = model.get_input_nodes()
    model_output_nodes = model.get_output_nodes()
    checks = onnx_test.get("import_check", {})
    nb_nodes = checks.get(
        "nb_nodes", len(onnx_model.graph.node) + len(onnx_model.graph.initializer)
    )
    nb_inputs = checks.get("nb_inputs", len(onnx_model.graph.input))
    nb_outputs = checks.get("nb_outputs", len(onnx_model.graph.output))
    nb_input_nodes = checks.get("nb_input_nodes", len(onnx_model.graph.input))
    nb_output_nodes = checks.get("nb_output_nodes", len(onnx_model.graph.output))
    assert nb_nodes == len(model_nodes), "unexpected number of nodes"
    assert nb_inputs == len(model_inputs), "unexpected number of inputs"
    assert nb_outputs == len(model_outputs), "unexpected number of inputs"
    assert nb_input_nodes == len(model_input_nodes), "unexpected number of input nodes"
    assert nb_output_nodes == len(
        model_output_nodes
    ), "unexpected number of output nodes"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
