"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import pytest
from pathlib import Path
import re
import os

import onnx

import aidge_core
import aidge_onnx

from utils.onnx_helpers import onnx_clone_model_with_initializers
from utils.onnx_test_cases import ONNXTestCases
from utils.pytest_helpers import PytestParamsGenerator

# Set to 1 for dumping onnx and verbose import on aidge_core.load_onnx()
TEST_IMPORT_ONNX_VERBOSE = bool(int(os.getenv("TEST_IMPORT_ONNX_VERBOSE", "0")))

# Use the per operator "node test cases" from ONNX sources
# Ref to ONNX Backend tests docs:
# https://github.com/onnx/onnx/blob/main/docs/OnnxBackendTest.md
ONNX_TESTS = ONNXTestCases(
    test_data_path="backend/test/data/node", fetch=True, fetch_tag="v1.16.2"
)

# Declared supported set, ops not in this set are ignored by default
ONNX_IMPORT_SUPPORTED_SET = set(aidge_onnx.node_import.supported_operators())

# Set to true shows unsupported operators as skipped, otherwise filtered out
ONNX_NOT_SUPPORTED_AS_SKIP = bool(
    int(os.getenv("TEST_ONNX_NOT_SUPPORTED_AS_SKIP", "0"))
)

# Leave empty or list exclusive ONNX operator names to test, for debug
DEBUG_ONLY_OPS = [
    # "add"
]

# XFAIL list for test_onnx_import_nodes()
# Attempt to keep oneliners (<= 88 cols)
XFAIL_ONNX_IMPORT_NODES = [
    (re.compile("averagepool_3d_dilations_.*"), "attributes unknown"),
    (re.compile("averagepool_2d_(dilations|ceil).*"), "attributes unknown"),
    (re.compile("averagepool_2d_(precomputed_)?pads_.*"), "attributes unknown"),
    (re.compile("gemm_.*(transposeA|all_attr)"), "supports only transA == 0"),
    (re.compile("gemm.*matrix_bias"), "2D bias not supported"),
    (re.compile("gemm.*(beta|alpha)"), "unsuppored attribute"),
    (re.compile("batchnorm_.*training_mode"), "does not support training_mode"),
    (re.compile("maxpool_.*with_argmax_2d.*_strides"), "storage_order != 0"),
    (re.compile("maxpool_.*dilations"), "does not support dilation_dims != 1"),
    (re.compile("split_equal_.*opset13"), "nb_outputs/split attributes"),
    (re.compile("lstm_batchwise"), "does not support layout attribute"),
    (re.compile("lstm_with_peepholes"), "does not support input #4"),
    (re.compile("pad/.*axes"), "pad axes not supported"),
    (re.compile("training_dropout"), "unsupported bool input"),
    (re.compile("^and/"), "unsupported bool input"),
    (re.compile("sum_(one|example)"), "support only 2 inputs"),
]

# SKIP list for test_onnx_import_nodes().
# Note that all non supported operators are automatically skipped.
# Attempt to keep oneliners (<= 88 cols)
SKIP_ONNX_IMPORT_NODES = []


def op_ident_func(op, model, *args):
    model = str(Path(model).parent).replace("test_", "")
    ident = f"{op}/{model}"
    if len(args) >= 1:
        dataset = str(Path(args[0]).name).replace("test_data_set_", "")
        ident = f"{ident}/{dataset}"
    return ident


def op_is_not_supported(op, model, *args):
    if op not in ONNX_IMPORT_SUPPORTED_SET:
        return f"ONNX {op}: not supported"
    return ""


def op_filter_func(op, model, *args):
    if DEBUG_ONLY_OPS and op not in DEBUG_ONLY_OPS:
        return f"ONNX {op}: not in debug list: filtered for debug"
    if not ONNX_NOT_SUPPORTED_AS_SKIP and op_is_not_supported(op, model, *args):
        return f"ONNX {op}: not supported: filtered"
    return ""


def generate_test_onnx_import_nodes():
    all_tests = [
        (op, model, dataset)
        for op in ONNX_TESTS.get_op_types()
        for model, dataset in ONNX_TESTS.get_type_simple_models_datasets(op)
    ]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=op_ident_func,
        skips=SKIP_ONNX_IMPORT_NODES,
        xfails=XFAIL_ONNX_IMPORT_NODES,
        filters_func=op_filter_func,
        skips_func=op_is_not_supported,
    ).generate()
    return params


@pytest.mark.parametrize(
    "onnx_op, onnx_model_path, onnx_dataset_path",
    generate_test_onnx_import_nodes(),
)
def test_onnx_import_nodes(onnx_op, onnx_model_path, onnx_dataset_path, tmpdir):
    model_path = ONNX_TESTS.test_path / onnx_model_path
    model_with_producers_path = Path(tmpdir) / "model_with_producers.onnx"
    onnx_model = onnx.load(str(model_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_model_path}:\n{onnx_model}")
    onnx_inputs, _ = ONNX_TESTS.onnx_load_dataset(onnx_dataset_path)

    onnx_clone_model_with_initializers(
        model_with_producers_path,
        model_path,
        onnx_inputs,
    )
    onnx_model_cst = onnx.load(str(model_with_producers_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model with producers {onnx_model_path}:\n{onnx_model_cst}")
    model = aidge_onnx.load_onnx(
        str(model_with_producers_path), verbose=TEST_IMPORT_ONNX_VERBOSE
    )
    for node in model.get_nodes():
        assert not isinstance(
            node.get_operator(), aidge_core.GenericOperatorOp
        ), f"GenericOperatorOp for node {node.name()} of type {node.type()}"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
