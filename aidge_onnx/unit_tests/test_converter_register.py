import unittest
import aidge_onnx
from aidge_core import Log, Level

class test_converter_register(unittest.TestCase):

    def setUp(self):
        pass
    def tearDown(self):
        pass

    def test_converter_register(self):
        Log.set_console_level(Level.Warn)
        self.assertTrue( 'Add' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'add' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'And' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'and' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'ArgMax' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'argmax' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'AvgPooling1D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'AvgPooling2D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'AvgPooling3D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'averagepool' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'BatchNorm2D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'batchnorm' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'BitShift' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'bitshift' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Clip' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'clip' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Concat' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'concat' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'constant' in aidge_onnx.node_import.supported_operators())

        self.assertTrue('ConstantOfShape' in aidge_onnx.node_export.supported_operators())
        self.assertTrue('constantofshape' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Conv2D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'conv' in aidge_onnx.node_import.supported_operators())

        self.assertTrue('div' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'dropout' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Erf' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'erf' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'FC' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'Gather' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'gather' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'gemm' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'GlobalAveragePooling' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'globalaveragepool' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Identity' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'identity' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'leakyrelu' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'lstm' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'matmul' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'MaxPooling2D' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'maxpool' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Mul' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'mul' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'PaddedAvgPooling2D' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'PaddedConv2D' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'PaddedConvDepthWise2D' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'PaddedMaxPooling2D' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'Producer' in aidge_onnx.node_export.supported_operators())

        self.assertTrue( 'ReduceMean' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'reducemean' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'ReduceSum' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'reducesum' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'pow' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'ReLU' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'relu' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Reshape' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'reshape' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'sigmoid' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Slice' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'slice' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'sqrt' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Softmax' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'softmax' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Squeeze' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'squeeze' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Sub' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'sub' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'tanh' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Transpose' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'transpose' in aidge_onnx.node_import.supported_operators())

        self.assertTrue( 'Unsqueeze' in aidge_onnx.node_export.supported_operators())
        self.assertTrue( 'unsqueeze' in aidge_onnx.node_import.supported_operators())

    def test_registering(self):
        aidge_onnx.node_import.register_import("my_op", lambda x : x)
        self.assertTrue('my_op' in aidge_onnx.node_import.supported_operators())
        aidge_onnx.node_export.register_export("my_op", lambda x : x)
        self.assertTrue('my_op' in aidge_onnx.node_export.supported_operators())

if __name__ == '__main__':
    unittest.main()
