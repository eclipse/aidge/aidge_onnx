"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import pytest
from pathlib import Path
import re
import os

import onnx

import aidge_core
import aidge_onnx

from utils.onnx_test_cases import ONNXTestCases
from utils.pytest_helpers import PytestParamsGenerator

# Set to 1 for dumping onnx and verbose import on aidge_onnx.load_onnx()
TEST_IMPORT_ONNX_VERBOSE = bool(int(os.getenv("TEST_IMPORT_ONNX_VERBOSE", "0")))

# Use the light model test cases from ONNX sources
# Ref to ONNX Backend tests docs:
# https://github.com/onnx/onnx/blob/main/docs/OnnxBackendTest.md
ONNX_TESTS = ONNXTestCases(
    test_data_path="backend/test/data/light", fetch=True, fetch_tag="v1.16.2"
)

# XFAIL list for test_onnx_import_models()
XFAIL_ONNX_IMPORT_MODELS = []

# SKIP list for test_onnx_import_models()
SKIP_ONNX_IMPORT_MODELS = []


def op_ident_func(model, *args):
    model = str(Path(model).stem)
    return model


def generate_test_onnx_import_models():
    all_tests = [
        (model, dataset)
        for model in ONNX_TESTS.get_models()
        for dataset in ONNX_TESTS.get_model_datasets(model)
    ]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=op_ident_func,
        skips=SKIP_ONNX_IMPORT_MODELS,
        xfails=XFAIL_ONNX_IMPORT_MODELS,
    ).generate()
    return params


@pytest.mark.parametrize(
    "onnx_model_path, onnx_dataset_path", generate_test_onnx_import_models()
)
def test_onnx_import_models(onnx_model_path, onnx_dataset_path):
    model_path = ONNX_TESTS.test_path / onnx_model_path
    onnx_model = onnx.load(str(model_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_model_path}:\n{onnx_model}")
    model = aidge_onnx.load_onnx(str(model_path), verbose=TEST_IMPORT_ONNX_VERBOSE)
    for node in model.get_nodes():
        assert not isinstance(
            node.get_operator(), aidge_core.GenericOperatorOp
        ), f"GenericOperatorOp generated for node {node.name()} of type {node.type()}"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
