"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import re
from typing import List, Tuple, Optional, Any, Callable
import pytest

__all__ = [
    "PytestParamsGenerator",
]

Params = Tuple[Any, ...]
IdentFun = Callable[..., str]
PyIdentFun = Callable[..., Optional[str]]
ParamsList = List[Params]
ReasonsList = List[Tuple[re.Pattern, str]]
ReasonFun = Callable[..., str]


class PytestParamsGenerator:
    """
    Generate with the `generate()` method a lst of test suitable for passing
    to the `@pytest.mark.parametrize` decorator.

    Possibility to match XFAILED/SKIPPED and filter tests with regexp match
    on test identifier or callable function with test parameters.

    XFAIL_*/SKIP_* lists are used to track test cases status,
    i.e. reported by pytest as XFAILED (resp. SKIPPED)
    These are lists of pairs: (regexp on test ident, xfail/skip message)
    - for XFAIL_*: failing but for which we do want to report failure:
      - new test with known limitations or incomplete implementation
      - if a XFAIL test transits to XPASS, implementation is fixed, remove from list
    - for SKIP_*: tests which are not executed
      - not part of the specification
      - break the test suite (should normally be XFAIL but segfault for instance)
      - these ones are not executed, hence transition to PASS is manual
    """

    def __init__(
        self,
        params: ParamsList,
        ident_func: Optional[IdentFun] = None,
        xfails: Optional[ReasonsList] = None,
        xfails_func: Optional[ReasonFun] = None,
        skips: Optional[ReasonsList] = None,
        skips_func: Optional[ReasonFun] = None,
        filters: Optional[ReasonsList] = None,
        filters_func: Optional[ReasonFun] = None,
    ) -> None:
        self.params = list(params)
        self.xfails = xfails
        self.xfails_func = xfails_func
        self.skips = skips
        self.skips_func = skips_func
        self.filters = filters
        self.filters_func = filters_func
        if ident_func is None:
            self.ident_func = lambda *args: "/".join([str(arg) for arg in args])
            self.pyident_func = None
        else:
            self.ident_func = ident_func
            self.pyident_func = ident_func

    def _get_reason(
        self,
        reasons: Optional[ReasonsList],
        func: Optional[ReasonFun],
        params: Params,
        ident: str,
    ) -> str:
        reason = "" if func is None else func(*params)
        if reason:
            return reason
        if reasons is None:
            return ""
        if not ident:
            return ""
        for ident_re, reason in reasons:
            if ident_re.search(ident):
                return reason
        return ""

    def xfail_reason(self, params: Params, ident: str) -> str:
        """Returns possibly empty reason for XFAILED."""
        reason = self._get_reason(
            self.xfails, self.xfails_func, params=params, ident=ident
        )
        return reason

    def skip_reason(self, params: Params, ident: str) -> str:
        """Returns possibly empty reason for SKIPPED."""
        reason = self._get_reason(
            self.skips, self.skips_func, params=params, ident=ident
        )
        return reason

    def filters_reason(self, params: Params, ident: str) -> str:
        """Returns possibly empty reason for filtering the test. Not processed by pytest."""
        reason = self._get_reason(
            self.filters, self.filters_func, params=params, ident=ident
        )
        return reason

    def generate(self) -> List[Any]:
        """Returns the list of tests as pytest parameters for `@pytest.mark.parametrize`."""
        all_idents = [self.ident_func(*args) for args in self.params]
        all_skips = [self.skip_reason(*args) for args in zip(self.params, all_idents)]
        all_xfails = [self.xfail_reason(*args) for args in zip(self.params, all_idents)]
        all_filters = [
            self.filters_reason(*args) for args in zip(self.params, all_idents)
        ]

        all_marks = [
            (
                pytest.mark.skip(reason=skip)
                if skip
                else pytest.mark.xfail(reason=xfail)
                if xfail
                else ()
            )
            for skip, xfail in zip(all_skips, all_xfails)
        ]
        params = [
            pytest.param(
                *params,
                marks=marks,
                id=(
                    self.pyident_func(*params)
                    if self.pyident_func is not None
                    else None
                ),
            )
            for params, marks, filt in zip(self.params, all_marks, all_filters)
            if not filt
        ]
        return params
