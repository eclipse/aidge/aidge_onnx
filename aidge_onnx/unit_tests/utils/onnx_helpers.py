"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import numpy as np
import onnx

__all__ = [
    "onnx_from_test",
    "onnx_save_test",
    "onnx_clone_model_with_initializers",
    "onnx_generate_input_data",
]


def onnx_from_test(onnx_test):
    """Generate ONNX model from onnx_test dict specification."""
    env = onnx_test.get("vars", {})

    def eval_value(val):
        return eval(val, {}, env) if isinstance(val, str) else val

    def eval_array(array_vals):
        np_array = np.array(array_vals)
        return np.vectorize(eval_value)(array_vals) if np_array.size > 0 else np_array

    def eval_shape(shape):
        return tuple(eval_array(shape).tolist())

    def eval_attr(attr_val):
        return eval_array(attr_val).tolist()

    def np_dtype(dtype):
        return {"float": "float32", "double": "float64"}.get(dtype, dtype)

    onnx_initializers = [
        onnx.numpy_helper.from_array(
            (
                eval_array(value).astype(np_dtype(dtype))
                + np.zeros(eval_shape(shape), dtype=np_dtype(dtype))
            ),
            name,
        )
        for name, dtype, shape, value in onnx_test.get("initializers", [])
    ]
    onnx_inputs = [
        onnx.helper.make_tensor_value_info(
            name,
            getattr(onnx.TensorProto, dtype.upper()),
            eval_shape(shape),
        )
        for name, dtype, shape in onnx_test.get("inputs", [])
    ]
    onnx_outputs = [
        onnx.helper.make_tensor_value_info(
            name,
            getattr(onnx.TensorProto, dtype.upper()),
            eval_shape(shape),
        )
        for name, dtype, shape in onnx_test.get("outputs", [])
    ]
    onnx_nodes = []
    for node in onnx_test.get("nodes", []):
        onnx_node = onnx.helper.make_node(
            op_type=node["op"],
            inputs=node["inputs"],
            outputs=node["outputs"],
        )
        for attr, value in node.get("attributes", {}).items():
            onnx_node.attribute.append(
                onnx.helper.make_attribute(attr, eval_attr(value))
            )
        onnx_nodes.append(onnx_node)

    onnx_graph = onnx.helper.make_graph(
        nodes=onnx_nodes,
        initializer=onnx_initializers,
        name=onnx_test["name"],
        inputs=onnx_inputs,
        outputs=onnx_outputs,
    )
    onnx_model = onnx.helper.make_model(
        onnx_graph, producer_name="aidge_onnx/unit_tests", producer_version="0.1.0"
    )
    return onnx_model


def onnx_save_test(onnx_test, onnx_path):
    """Save ONXX from onnx_test dict specification."""
    onnx_model = onnx_from_test(onnx_test)
    onnx.save(onnx_model, str(onnx_path))


def onnx_clone_model_with_initializers(out_onnx_path, onnx_path, inputs):
    """
    Generate a new ONNX model file from the given model and the
    given inputs map (name -> TensorValue) with inputs inlined as
    ONNX initializers.
    This is useful to test some operators which require constant inputs
    values.
    Note that inputs which are not tensors (such as sequences) are ignored
    """
    onnx_model = onnx.load(str(onnx_path))
    new_initializers = []
    for inp, val in zip(onnx_model.graph.input, inputs.values()):
        if not inp.type.HasField("tensor_type"):
            continue
        init = onnx.helper.make_tensor(
            name=inp.name,
            dims=val.dims,
            data_type=val.data_type,
            vals=val.raw_data,
            raw=True,
        )
        new_initializers.append(init)
    new_onnx_graph = onnx.helper.make_graph(
        nodes=onnx_model.graph.node,
        initializer=list(onnx_model.graph.initializer) + new_initializers,
        name=onnx_model.graph.name,
        inputs=onnx_model.graph.input,
        outputs=onnx_model.graph.output,
    )
    new_onnx_model = onnx.helper.make_model(
        new_onnx_graph,
        producer_name=onnx_model.producer_name,
        producer_version=onnx_model.producer_version,
        opset_imports=onnx_model.opset_import,
    )
    onnx.save(new_onnx_model, str(out_onnx_path))


def onnx_generate_input_data(onnx_input):
    """Generates fixed data for float tensors."""
    assert onnx_input.type.HasField("tensor_type"), "expected TensorType input"
    assert onnx_input.type.tensor_type.elem_type == 1, "expected Float32 Tensor input"
    shape = tuple(
        d.dim_value if d.HasField("dim_value") else 1
        for d in onnx_input.type.tensor_type.shape.dim
    )
    n = np.prod(shape).astype("int")
    return (np.arange(n) / n).reshape(shape).astype("float32")
