"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

from typing import Set

import aidge_core

__all__ = [
    "model_convert_aidge_nodes",
    "model_get_nodes_op_class",
]


def model_convert_aidge_nodes(model: aidge_core.GraphView) -> None:
    """Replace all generic nodes of type aidge::<opetasor> with actual aidge <operator> node."""
    aidge_pfx = "aidge::"
    for node in [
        node for node in model.get_nodes() if node.type().startswith(aidge_pfx)
    ]:
        aidge_type = node.type()[len(aidge_pfx) :]
        assert hasattr(
            aidge_core, aidge_type
        ), f"undefined constructore for type: {aidge_type}"
        aidge_attrs = node.get_operator().attr.dict()
        aidge_node = getattr(aidge_core, aidge_type)(**aidge_attrs)
        replaced = model.replace({node}, {aidge_node})
        assert replaced, f"could not replace node {node.name()} of type {node.type()}"


def model_get_nodes_op_class(
    model: aidge_core.GraphView, op_class: str
) -> Set[aidge_core.Node]:
    """Return nodes of the specified op class."""
    cls = getattr(aidge_core, op_class, None)
    return {node for node in model.get_nodes() if isinstance(node.get_operator(), cls)}
