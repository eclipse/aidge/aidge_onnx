"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

from pathlib import Path
import glob
import re
import string
import shutil
import logging
import requests
import tempfile
from zipfile import ZipFile

import onnx
from typing import Optional, Union, List, Tuple, Dict

__all__ = [
    "ONNXSourceCache",
    "ONNXTestCases",
]

logger = logging.getLogger(__name__)


class ONNXSourceCache:
    """
    Manages fetch and cache of ONNX sources code based on
    an unique identification from `repo_url` and `tag`.
    The `source_path` attribute is a `Path` object to
    the ONNX source tree.
    The source tree is only available after a call to `fetch()`.
    """

    ONNX_GITHUB_REPO_URL = "https://github.com/onnx/onnx"
    DEFAULT_REPO_URL = ONNX_GITHUB_REPO_URL
    DEFAULT_TAG = "1.16.2"
    DEFAULT_CACHE_PATH = Path(".cache_onnx_sources")

    logger = logging.getLogger(f"{__name__}.ONNXSourceCache")

    def __init__(
        self,
        tag: Optional[str] = None,
        repo_url: Optional[str] = None,
        cache_path: Optional[Union[Path, str]] = None,
    ) -> None:
        self.tag = tag if tag is not None else self.DEFAULT_TAG
        self.repo_url = repo_url if repo_url is not None else self.DEFAULT_REPO_URL
        self.download_url = f"{self.repo_url}/archive/refs/tags/{self.tag}.zip"
        self.cache_path = Path(
            cache_path if cache_path is not None else self.DEFAULT_CACHE_PATH
        )
        self.cache_id = self._get_cache_id()
        self.source_path = self.cache_path / self.cache_id / f"onnx-{self.tag[1:]}"
        self.archive_path = self.cache_path / self.cache_id / f"onnx-{self.tag[1:]}.zip"

    def _get_cache_id(self) -> str:
        unique = f"cache_{self.repo_url}_{self.tag}"
        return re.sub(rf"[^{string.ascii_letters}{string.digits}]", "_", unique)

    def _fetch_sources(self) -> None:
        cache_entry_path = self.cache_path / self.cache_id
        if cache_entry_path.exists():
            return
        self.cache_path.mkdir(parents=True, exist_ok=True)
        tmp_path = Path(
            tempfile.mkdtemp(prefix=f"tmp_{self.cache_id}", dir=self.cache_path)
        )
        try:
            archive_file = tmp_path / self.archive_path.name
            self.logger.debug(
                f"Fetch sources: from {self.download_url} to {archive_file}"
            )
            response = requests.get(self.download_url, stream=True)
            response.raise_for_status()
            with open(archive_file, "wb") as outf:
                for chunk in response.iter_content(chunk_size=None):
                    outf.write(chunk)
            self.logger.debug(f"Unarchive sources: from {archive_file} to {tmp_path}")
            with ZipFile(str(archive_file)) as inz:
                inz.extractall(path=str(tmp_path))
            self.logger.debug(
                f"Atomic renaming to cache entry: from {tmp_path} to {cache_entry_path}"
            )
            tmp_path.rename(cache_entry_path)
        finally:
            if tmp_path.exists():
                shutil.rmtree(tmp_path)

    def fetch(self) -> Path:
        """
        Fetch and cache the requested source tag.
        Returns the ONNX sources path.
        """
        self._fetch_sources()
        return self.source_path


class ONNXTestCases:
    """
    Manages test cases as provided by ONNX sources, generally in `backend/test/data/*`.
    Provides interface to discover and enumerate models and datasets.
    Provides interface to map from models to list of operator types used in the model
    and from operator types to models including the operator type.
    """

    ONNX_MODULE_PATH = Path(onnx.__file__).parent
    ONNX_TEST_NODES_PATH = Path("backend/test/data/node")
    DEFAULT_TEST_DATA_PATH = ONNX_TEST_NODES_PATH
    DEFAULT_ONNX_PATH = ONNX_MODULE_PATH

    def __init__(
        self,
        test_data_path: Optional[Union[Path, str]] = None,
        onnx_path: Optional[Union[Path, str]] = None,
        fetch: bool = False,
        fetch_tag: Optional[str] = None,
        fetch_repo_url: Optional[str] = None,
        fetch_cache_path: Optional[Union[Path, str]] = None,
    ) -> None:
        self._fetch = fetch
        if self._fetch:
            self._source_cache = ONNXSourceCache(
                tag=fetch_tag, repo_url=fetch_repo_url, cache_path=fetch_cache_path
            )
            self.onnx_path = Path(self._source_cache.source_path) / "onnx"
        else:
            self.onnx_path = Path(onnx_path if onnx_path else self.DEFAULT_ONNX_PATH)
            if (self.onnx_path / "onnx").exists():
                self.onnx_path = self.onnx_path / "onnx"
        self.test_data_path = Path(
            test_data_path
            if test_data_path is not None
            else self.DEFAULT_TEST_DATA_PATH
        )
        self.test_path = self.onnx_path / self.test_data_path
        # tests info is built lazily, always call self._build_test_info() before accessing maps below
        self._built_test_info = False
        # maps model name to list of operator types used in the model
        self._model_types_map: Dict[str, List[str]] = {}
        # maps operator type to list of models using this operator type
        self._type_models_map: Dict[str, List[str]] = {}
        # maps model name to list of available datasets names
        self._model_datasets_map: Dict[str, List[str]] = {}

    def _fetch_sources(self) -> None:
        if self._fetch:
            self._source_cache.fetch()

    def _glob(self, glob_str: str) -> List[str]:
        self._fetch_sources()
        root_dir = str(self.test_path)
        glob_path = f"{root_dir}/{glob_str}"
        return [path[len(f"{root_dir}/") :] for path in glob.glob(glob_path)]

    def _fullpath(self, path_str: str) -> str:
        return str(self.test_path / path_str)

    def _get_models(self) -> List[str]:
        models = self._glob("*.onnx")
        models += self._glob("test_*/*.onnx")
        assert models, f"No model found in test path: {self.test_path}"
        return models

    def _get_model_datasets(self, model: str) -> List[str]:
        # Check first node like test case which have dataset in ./test_data_set_*/ dir
        datasets = self._glob(f"{Path(model).parent}/test_data_set_*")
        if datasets:
            return datasets
        # Check model with no inputs cases, where outputs are ./<model_name>_output_*.pb
        dataset = str(Path(model).parent / Path(model).stem)
        if self._glob(f"{dataset}_*.pb"):
            return [dataset]
        assert datasets, f"No dataset found for: {model} at path: {self.test_path}"

    def _build_tests_info(self) -> None:
        if self._built_test_info:
            return
        onnx_paths = self._get_models()
        self._model_types_map = {}
        self._type_models_map = {}
        self._model_datasets_map = {}
        for onnx_path in onnx_paths:
            onnx_model = onnx.load(str(self.test_path / onnx_path))
            op_types = []
            for onnx_node in onnx_model.graph.node:
                op_types.append(onnx_node.op_type.lower())
            op_types = list(dict.fromkeys(op_types))
            for op_type in op_types:
                if op_type not in self._type_models_map:
                    self._type_models_map[op_type] = []
                self._type_models_map[op_type].append(onnx_path)
            self._model_types_map[onnx_path] = op_types
            self._model_datasets_map[onnx_path] = self._get_model_datasets(onnx_path)
        self._built_test_info = True

    def _get_simple_model_type_map(self) -> Dict[str, str]:
        self._build_tests_info()
        return {
            model: op_types[0]
            for model, op_types in self._model_types_map.items()
            if len(op_types) == 1
        }

    def get_type_models(self, op_type: str) -> List[str]:
        """Get models names including the given op type."""
        self._build_tests_info()
        return self._type_models_map.get(op_type, [])

    def get_type_models_datasets(self, op_type: str) -> List[Tuple[str, str]]:
        """Get models datasets name including the given op type."""
        model_dataset = []
        for model in self.get_type_models(op_type):
            for dataset in self.get_model_datasets(model):
                model_dataset.append((model, dataset))
        return model_dataset

    def get_type_simple_models(self, op_type: str) -> List[str]:
        """Get simple node models names for the given op type. I.e. models with just this op type node."""
        models = self.get_type_models(op_type)
        return [model for model in models if len(self.get_model_op_types(model)) == 1]

    def get_type_simple_models_datasets(self, op_type: str) -> List[Tuple[str, str]]:
        """Get simple node models datasets names for the given op type."""
        model_dataset = []
        for model in self.get_type_simple_models(op_type):
            for dataset in self.get_model_datasets(model):
                model_dataset.append((model, dataset))
        return model_dataset

    def get_op_types(self) -> List[str]:
        """Get sorted list of all unique op types present in the test suite."""
        self._build_tests_info()
        return list(sorted(self._type_models_map.keys()))

    def get_models(self) -> List[str]:
        """Get sorted list of all models names prsent in the test suite."""
        self._build_tests_info()
        return list(sorted(self._model_types_map.keys()))

    def get_model_op_types(self, model: str) -> List[str]:
        """Get sorted unique list of op type in the given model name."""
        self._build_tests_info()
        return list(sorted(self._model_types_map[model]))

    def get_model_datasets(self, model: str) -> List[str]:
        """Get available datasets names for the given model name."""
        self._build_tests_info()
        return self._model_datasets_map[model]

    def onnx_load_dataset(self, dataset: str):
        """
        Load the dataset and returns the ordered inputs and outputs map.
        """
        inputs, outputs = [], []
        glob_str = (
            f"{dataset}/*.pb"
            if Path(self._fullpath(dataset)).is_dir()
            else f"{dataset}_*.pb"
        )
        for fname in self._glob(glob_str):
            tensor = onnx.TensorProto()
            with open(self._fullpath(fname), "rb") as inf:
                tensor.ParseFromString(inf.read())
            ident = Path(fname).stem.rsplit("_", 2)[-2:]
            idx = int(ident[1])
            if ident[0] == "input":
                inputs.append((idx, tensor))
            elif ident[0] == "output":
                outputs.append((idx, tensor))
            else:
                assert False, f"unexpected dataset file: {fname}"
        inputs_map = {
            tensor.name: tensor for _, tensor in sorted(inputs, key=lambda x: x[0])
        }
        outputs_map = {
            tensor.name: tensor for _, tensor in sorted(outputs, key=lambda x: x[0])
        }
        return inputs_map, outputs_map

    def print_summary(self) -> None:
        """
        Print summary information for the test suite.
        """
        self._build_tests_info()
        op_types = self.get_op_types()
        model_types_map = self._model_types_map
        simple_model_type_map = self._get_simple_model_type_map()
        print("summary:")
        print(f"  op_type_count: {len(op_types)}")
        print(f"  models_count: {len(model_types_map)}")
        print(f"  simple_models_count: {len(simple_model_type_map)}")

    def dump(self) -> None:
        """
        Dump full test suite information as models map and optype map.
        The dump format a valid parsable YAML output.
        """
        self._build_tests_info()
        op_types = self.get_op_types()
        model_types_map = self._model_types_map
        simple_model_type_map = self._get_simple_model_type_map()
        print("dump:")
        print(f"  op_type_count: {len(op_types)}")
        print(f"  models_count: {len(model_types_map)}")
        print(f"  simple_models_count: {len(simple_model_type_map)}")
        print("  op_types:")
        for op_type in op_types:
            onnx_models = self.get_type_models(op_type)
            onnx_simple_models = self.get_type_simple_models(op_type)
            onnx_datasets = self.get_type_models_datasets(op_type)
            onnx_simple_datasets = self.get_type_simple_models_datasets(op_type)
            print(f"    - op_type: {op_type}")
            print(f"      models: {len(onnx_models)}")
            print(f"      datasets: {len(onnx_datasets)}")
            print(f"      single_models: {len(onnx_simple_models)}")
            print(f"      single_datasets: {len(onnx_simple_datasets)}")
        print("  models:")
        for model in model_types_map:
            onnx_op_types = self.get_model_op_types(model)
            onnx_datasets = self.get_model_datasets(model)
            print(f"    - model: {model}")
            print(f"      op_types: {len(onnx_op_types)}")
            print(f"      datasets: {len(onnx_datasets)}")


def main():
    """
    Test program to fetch and print summary of ONNX test suite.
    """
    default_test_path, default_tag = str(ONNXTestCases.ONNX_TEST_NODES_PATH), "v1.16.2"
    import argparse

    class Formatter(
        argparse.RawTextHelpFormatter, argparse.ArgumentDefaultsHelpFormatter
    ):
        """Inherit both formatters"""

    parser = argparse.ArgumentParser(
        description="""
        Generate tests information from ONNX data tests. This is for test usage only.

        Will automatically fetch ONNX sources into a local cache directory given the
        option --tag to identify the ONNX version.
        Otherwise, option --module can be used to query the currently installed
        onnx module, or option --onnx-path can be used to point to an ONNX
        source tree.
        Use option --dump to have a full dump of the models and operator types informations.

        For instance generate summary information for ONNX node tests:
          python onnx_test_cases.py [--tag v1.16.2] [backend/test/data/node]
          summary:
            op_type_count: 188
            models_count: 1282
            simple_models_count: 1045

        Or generate summary information for ONNX models simple tests with:
          python onnx_test_cases.py backend/test/data/simple
          summary:
            op_type_count: 16
            models_count: 23
            simple_models_count: 13
        """,
        formatter_class=Formatter,
    )
    parser.add_argument(
        "--tag", type=str, default=default_tag, help="ONNX sources tag to fetch"
    )
    parser.add_argument(
        "--module",
        action="store_true",
        help="Optionally use locally installed onnx module instead of fetching sources",
    )
    parser.add_argument(
        "--onnx-path",
        type=str,
        help="Optionally use path to ONNX sources tree instead of fetching sources",
    )
    parser.add_argument(
        "--dump",
        action="store_true",
        help="Dump full information on models and operator types instead of just a summary",
    )
    parser.add_argument("--debug", action="store_true", help="Enable debug output")
    parser.add_argument(
        "test_data_path",
        nargs="?",
        default=default_test_path,
        help="Optional test data path in onnx sources, or nodes test suite",
    )
    args = parser.parse_args()

    logging.basicConfig()
    if args.debug:
        logger.setLevel(logging.DEBUG)

    if args.module:
        logger.debug("Using installed module")
        test_cases = ONNXTestCases(args.test_data_path)
    elif args.onnx_path:
        logger.debug("Using installed sources")
        test_cases = ONNXTestCases(args.test_data_path, onnx_path=args.onnx_path)
    else:
        logger.debug("Fetching sources")
        test_cases = ONNXTestCases(args.test_data_path, fetch=True, fetch_tag=args.tag)

    logger.debug(
        f"Install tests from onnx: {test_cases.onnx_path}: subdir: {test_cases.test_data_path}"
    )
    if args.dump:
        test_cases.dump()
    else:
        test_cases.print_summary()


if __name__ == "__main__":
    main()
