"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import numpy as np
from pathlib import Path
import re
import pytest

import onnx

import aidge_core
import aidge_backend_cpu  # noqa: F401
import aidge_onnx

from utils.pytest_helpers import PytestParamsGenerator
from utils.onnx_helpers import onnx_from_test
from utils.aidge_helpers import model_convert_aidge_nodes, model_get_nodes_op_class

from test_unit_cases_import import (
    TEST_IMPORT_CASES,
    TEST_IMPORT_ONNX_VERBOSE,
    XFAIL_TEST_IMPORT,
    SKIP_TEST_IMPORT,
)

XFAIL_TEST_IMPORT_FORWARD = [
    (re.compile("^loop_inc/0"), "Memorize: invalid iterations: expected 10 != 1"),
]

SKIP_TEST_IMPORT_FORWARD = []


def generate_test_import_forward():
    all_tests = [
        (t["name"], t, d)
        for t in TEST_IMPORT_CASES
        for d in range(len(t.get("datasets", [])))
    ]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=lambda name, test, dataset: f"{name}/{dataset}",
        skips=SKIP_TEST_IMPORT + SKIP_TEST_IMPORT_FORWARD,
        xfails=XFAIL_TEST_IMPORT + XFAIL_TEST_IMPORT_FORWARD,
    ).generate()
    return params


@pytest.mark.parametrize(
    "onnx_name, onnx_test, dataset", generate_test_import_forward()
)
def test_import_forward(onnx_name, onnx_test, dataset, tmpdir):
    onnx_path = Path(tmpdir) / f"{onnx_name}.onnx"
    onnx_model = onnx_from_test(onnx_test)
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_name}:\n{onnx_model}")
    onnx.save(onnx_model, str(onnx_path))

    model = aidge_onnx.load_onnx(str(onnx_path), verbose=TEST_IMPORT_ONNX_VERBOSE)
    model_convert_aidge_nodes(model)
    generics = model_get_nodes_op_class(model, "GenericOperatorOp")
    assert (
        len(generics) == 0
    ), f"GenericOperatorOp generated for nodes {[node.name() for node in generics]}"

    np_inputs = [
        np.array(inp[0], dtype=inp[1])
        for inp in onnx_test["datasets"][dataset]["inputs"]
        if inp is not None
    ]
    np_outputs = [
        np.array(out[0], dtype=out[1])
        for out in onnx_test["datasets"][dataset]["outputs"]
        if out is not None
    ]

    model.set_backend("cpu")
    scheduler = aidge_core.SequentialScheduler(model)
    inputs = [aidge_core.Tensor(input_val) for input_val in np_inputs]
    scheduler.forward(data=inputs)
    aidge_outputs = [
        out[0].get_operator().get_output(out[1]) for out in model.get_ordered_outputs()
    ]
    aidge_np_outputs = [np.array(out) for out in aidge_outputs]

    assert len(np_outputs) <= len(aidge_np_outputs), "unexpected number of outputs"

    for idx, (out, aidge_out) in enumerate(zip(np_outputs, aidge_np_outputs)):
        assert (
            out.dtype == aidge_out.dtype
        ), f"output #{idx} dtypes do not match: {out.dtype} != {aidge_out.dtype}"
        assert (
            out.shape == aidge_out.shape
        ), f"output #{idx} shapes do not match: {out.shape} != {aidge_out.shape}"
        assert np.allclose(
            out, aidge_out
        ), f"output #{idx} do not match: {out} != {aidge_out}"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
