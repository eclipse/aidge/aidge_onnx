"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""

import numpy as np
import pytest
from pathlib import Path
import re

import onnx
import onnxruntime

import aidge_core
import aidge_backend_cpu  # noqa: F401
import aidge_onnx

from utils.onnx_helpers import onnx_clone_model_with_initializers
from utils.pytest_helpers import PytestParamsGenerator

# This test is linked to test_onnx_nodes_import.py test which does the import
# XFAIL/SKIP lists for import are added to this test XFAIL/SKIP lists
from test_onnx_nodes_import import (
    TEST_IMPORT_ONNX_VERBOSE,
    ONNX_TESTS,
    op_ident_func,
    op_is_not_supported,
    op_filter_func,
    XFAIL_ONNX_IMPORT_NODES,
    SKIP_ONNX_IMPORT_NODES,
)


# XFAIL list for test_onnx_import_nodes_forward(),
# added to the XFAIL_ONNX_IMPORT_NODES list
# Attempt to keep oneliners (<= 88 cols)
XFAIL_ONNX_IMPORT_NODES_FORWARD = [
    # ONNX runtime error (not related to Aidge)
    (re.compile("add.*_uint8/"), "ONNX runtime error"),
    (re.compile("div.*_uint8/"), "ONNX runtime error"),
    (re.compile("mul.*_uint8/"), "ONNX runtime error"),
    (re.compile("sub.*_uint8/"), "ONNX runtime error"),
    (re.compile("pow_.*_uint(32|64)/"), "ONNX runtime error"),
    (re.compile("identity_sequence/"), "ONNX runtime error"),
    (re.compile("identity_opt/"), "ONNX runtime error"),
    (re.compile("bitshift.*_uint16"), "ONNX runtime error"),
    # issues with inputs/outputs tensors creation/retrieval
    (re.compile("dropout_default_mask"), "identity operator has 1 output"),
    (re.compile("maxpool_with_argmax_2d_pre.*_pads"), "operator has 1 output"),
    # issues in forward()
    (re.compile("gemm_default_(scalar|single.*).*bias"), "unexpected bias size"),
    (re.compile("slice_default_axes/"), "input #3 Axes required"),
    (re.compile("reshape_allowzero_reordered/"), "input #0 required"),
    (re.compile("slice_end_out_of_bounds/"), "ROI attribute invalid"),
    (re.compile("shape_clip_end/"), "start and end must be < 3"),
    (re.compile("shape_start_negative"), "invalid roi"),
    # cpu forward not implemented
    (re.compile("averagepool_[13]d_default/"), "cpu forward not implemented"),
    (re.compile("maxpool_[13]d_default/"), "cpu forward not implemented"),
    (re.compile("maxpool_2d_uint8/"), "cpu forward not implemented"),
    (re.compile("pow_.*(int.*_int.*|int.*_float.*|float.*_int.*)/"), "mixed int/float"),
    (re.compile("pow_bcast_scalar/"), "mixed float not implemented"),
    (re.compile("^bitshift/"), "cpu forward not implemented"),
    (re.compile("^slice/"), "cpu forward not implemented"),
    (re.compile("^pad/"), "cpu forward not implemented"),
    (re.compile("split_zero_size_splits"), "split: no input #0"),
    (re.compile("clip/(?!clip_default_inbound)"), "types not implemented"),
    # output dtype errors
    (re.compile("constantofshape_int_(shape_)?zero"), "output dtype mismatch"),
    (re.compile("clip_default_int8"), "output dtype mismatch"),
    (re.compile("/argmax"), "output type is not int64"),
    (re.compile("/shape"), "output type is not int64"),
    # output shape errors
    (re.compile("globalaveragepool_precomputed"), "output shape mismatch"),
    (re.compile("maxpool_2d_precomputed_same"), "output shape mismatch"),
    (re.compile("conv_with_autopad_same"), "padding incorrect"),
    (re.compile("conv_with_stride"), "padding incorrect"),
    (re.compile("slice_neg_steps/"), "negative step"),
    (re.compile("slice_2d_uneven_split/"), "uneven split"),
    (re.compile("reduce_sum_empty_set"), "generate empty shape"),
    # output values errors
    (re.compile("averagepool_2d_pads/"), "output values mismatch"),
    (re.compile("averagepool_2d_pre.*_same_upper/"), "output values mismatch"),
    (re.compile("averagepool_2d_pre.*_pads/"), "output values mismatch"),
    (re.compile("averagepool_2d_same_lower/"), "output values mismatch"),
    (re.compile("averagepool_2d_same_upper/"), "output values mismatch"),
    (re.compile("globalaveragepool/"), "output values mismatch"),
    (re.compile("maxpool_2d_same_lower/"), "output values mismatch"),
    (re.compile("maxpool_2d_pads/"), "output values mismatch"),
    (re.compile("maxpool_2d_ceil/"), "output values mismatch"),
    (re.compile("maxpool_2d_ceil_.*_reduce_by_one/"), "output values mismatch"),
    (re.compile("maxpool_2d_same_upper/"), "output values mismatch"),
    (re.compile("split_[12]d_uneven_split_opset18/"), "output values mismatch"),
    (re.compile("gridsample/"), "output values mismatch"),
]

# SKIP list for test_onnx_import_nodes_forward(),
# added to the SKIP_ONNX_IMPORT_NODES list
# Note that there may be tests which segfault or run into infinite loops,
# they should be put there instead of the XFAIL list
# Note that these tests will not be reported as XPASS when passing,
# comment them to verify
# Attempt to keep oneliners (<= 88 cols)
SKIP_ONNX_IMPORT_NODES_FORWARD = [
    (re.compile("^concat/concat_.*_negative_"), "infinite loop in forward"),
    (re.compile("^slice/slice_start_out_of_bounds"), "python segfault"),
    (re.compile("^lstm/"), "python segfault"),
    (re.compile("^argmax/argmax_negative_axis_"), "python segfault"),
    (re.compile("reduce_sum_empty_set"), "invalid pointer"),
]


def generate_test_onnx_import_nodes_forward():
    all_tests = [
        (op, model, dataset)
        for op in ONNX_TESTS.get_op_types()
        for model, dataset in ONNX_TESTS.get_type_simple_models_datasets(op)
    ]
    params = PytestParamsGenerator(
        all_tests,
        ident_func=op_ident_func,
        skips=SKIP_ONNX_IMPORT_NODES + SKIP_ONNX_IMPORT_NODES_FORWARD,
        xfails=XFAIL_ONNX_IMPORT_NODES + XFAIL_ONNX_IMPORT_NODES_FORWARD,
        filters_func=op_filter_func,
        skips_func=op_is_not_supported,
    ).generate()
    return params


@pytest.mark.parametrize(
    "onnx_op, onnx_model_path, onnx_dataset_path",
    generate_test_onnx_import_nodes_forward(),
)
def test_onnx_import_nodes_forward(onnx_op, onnx_model_path, onnx_dataset_path, tmpdir):
    model_path = ONNX_TESTS.test_path / onnx_model_path
    model_with_producers_path = Path(tmpdir) / "model_with_producers.onnx"
    onnx_model = onnx.load(str(model_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model {onnx_model_path}:\n{onnx_model}")
    onnx_inputs, onnx_outputs = ONNX_TESTS.onnx_load_dataset(onnx_dataset_path)
    onnx_session = onnxruntime.InferenceSession(model_path)
    onnx_initializers = {ini.name: ini for ini in onnx_model.graph.initializer}
    np_inputs_map = {
        inp.name: onnx.numpy_helper.to_array(onnx_inputs[inp.name])
        for inp in onnx_model.graph.input
        if inp.name not in onnx_initializers
    }
    outputs_names = [out.name for out in onnx_model.graph.output]
    onnx_outputs = onnx_session.run(outputs_names, np_inputs_map)

    onnx_clone_model_with_initializers(
        model_with_producers_path,
        model_path,
        onnx_inputs,
    )
    onnx_model_cst = onnx.load(str(model_with_producers_path))
    if TEST_IMPORT_ONNX_VERBOSE:
        print(f"ONNX model with producers {onnx_model_path}:\n{onnx_model_cst}")
    model = aidge_onnx.load_onnx(
        str(model_with_producers_path), verbose=TEST_IMPORT_ONNX_VERBOSE
    )
    model.set_backend("cpu")
    scheduler = aidge_core.SequentialScheduler(model)

    onnx_initializers_cst = {ini.name: ini for ini in onnx_model_cst.graph.initializer}
    np_inputs_map_cst = {
        inp.name: onnx.numpy_helper.to_array(onnx_inputs[inp.name])
        for inp in onnx_model_cst.graph.input
        if inp.name not in onnx_initializers_cst
    }

    aidge_inputs = [aidge_core.Tensor(v) for v in np_inputs_map_cst.values()]
    scheduler.forward(forward_dims=False, data=aidge_inputs)
    aidge_outputs = [
        np.array(out[0].get_operator().get_output(out[1]))
        for out in model.get_ordered_outputs()
    ]

    for idx, (onnx_out, out) in enumerate(zip(onnx_outputs, aidge_outputs)):
        assert (
            onnx_out.dtype == out.dtype
        ), f"out #{idx} dtypes mismatch: {onnx_out.dtype} != {out.dtype}"
        assert (
            onnx_out.shape == out.shape
        ), f"out #{idx} shapes mismatch: {onnx_out.shape} != {out.shape}"
        assert np.allclose(onnx_out, out), f"out #{idx} mismatch: {onnx_out} != {out}"


def main():
    import sys

    print(
        f"{sys.argv[0]}: Warning: skipped: run with: pytest {sys.argv[0]}",
        file=sys.stderr,
    )


if __name__ == "__main__":
    main()
