"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple
import numpy as np

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import
from aidge_onnx.utils import get_node_attributes

from aidge_core import Log
from aidge_onnx.utils import warn_unsupported_attr

@auto_register_import("averagepool")
def import_avg_pooling(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset=None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of Aidge nodes which constitute the input of the current node
    :type input_nodes: List[aidge_core.Node]
    :param opset: Indicate opset version of the ONNX model, default=None
    :type opset: int, optional
    """
    node_name = onnx_node.name if onnx_node.name else onnx_node.output[0]
    onnx_attrs = get_node_attributes(onnx_node, opset)
    avg_pool_attrs: dict = {}
    #attributes common to every opset of avgpool: auto_pad, kernel_shape, pads, strides

    if 'kernel_shape' in onnx_attrs:
        kernel_dims = onnx_attrs['kernel_shape']
        del onnx_attrs['kernel_shape']
        dimensions = len(kernel_dims)
    else:
        Log.warn("Warning: Operator 'AvgPool' must have 'kernel_shape' attribute. This node will be filled by a GenericOperator.")
        return None

    if 'strides' in onnx_attrs:
        avg_pool_attrs['stride_dims'] = onnx_attrs['strides']
        del onnx_attrs['strides']
    else:
        # If not present, the stride defaults is 1 along each spatial axis.
        avg_pool_attrs['stride_dims'] = [1] * dimensions

    avg_pool_attrs['padding_dims'] = [0] * 2*dimensions
    if 'pads' in onnx_attrs:
        # `pads` format should be as follow [x1_begin, x2_begin...x1_end, x2_end,...]
        for i in range(0, dimensions):
            avg_pool_attrs['padding_dims'][2*i] = onnx_attrs['pads'][i]
            avg_pool_attrs['padding_dims'][2*i+1] = onnx_attrs['pads'][dimensions-1+i]
        del onnx_attrs['pads']

    if 'auto_pad' in onnx_attrs and onnx_attrs['auto_pad'] in (b'NOTSET', b'SAME_UPPER', b'SAME_LOWER', b'VALID'):
        if onnx_attrs['auto_pad'] != b'NOTSET' and np.count_nonzero(avg_pool_attrs['padding_dims']) > 0:
            raise RuntimeError("Error: malformed ONNX: cannot have both non-zero 'pads' and 'auto_pad' different from 'NOTSET'.")

        for i,ele in enumerate(kernel_dims):
            padding = ele - avg_pool_attrs['stride_dims'][i]
            floorHalfPadding = padding // 2
            if onnx_attrs['auto_pad'] == b'SAME_UPPER':
                avg_pool_attrs['padding_dims'][2*i] = floorHalfPadding
                avg_pool_attrs['padding_dims'][2*i+1] = padding - floorHalfPadding
            elif onnx_attrs['auto_pad'] == b'SAME_LOWER':
                avg_pool_attrs['padding_dims'][2*i] = padding - floorHalfPadding
                avg_pool_attrs['padding_dims'][2*i+1] = floorHalfPadding
        del onnx_attrs['auto_pad']

    #attributes dependent on the operator's opset
    if opset >= 19:
        if 'dilations' in onnx_attrs:
            dilation_dims = onnx_attrs['dilations']
            del onnx_attrs['dilations']
        else:
            # If not present, the stride defaults is 1 along each spatial axis.
            dilation_dims = [1] * dimensions

        if np.count_nonzero(dilation_dims - np.array(1)) > 0:
            warn_unsupported_attr('dilations','AveragePool',opset,dilation_dims)
            return None

    if opset >= 10:
        if 'ceil_mode' in onnx_attrs:
            if onnx_attrs['ceil_mode'] != 0:
                warn_unsupported_attr('ceil_mode','AveragePool',opset,onnx_attrs['ceil_mode'])
                return None
            del onnx_attrs['ceil_mode']

    if opset >= 7:
        if 'count_include_pad' in onnx_attrs:
            if onnx_attrs['count_include_pad'] != 0:
                warn_unsupported_attr('count_include_pad','AveragePool',opset,onnx_attrs['count_include_pad'])
                return None
            del onnx_attrs['count_include_pad']


    if len(onnx_attrs) > 0:
        Log.warn(f"Warning: unsupported attribute(s): {onnx_attrs.keys()} for operator 'AveragePool' with opset {opset}.\nThis node will be filled by a GenericOperator.")
        return None

    #Usage of AvgPoolingOpxD or PaddedAvgPoolingOpxD
    op_aidge_class_name = f"AvgPooling{dimensions}D"
    op_aidge_constr_name = f"AvgPooling{dimensions}DOp"
    if np.count_nonzero(avg_pool_attrs['padding_dims']) > 0:
        op_aidge_class_name = "Padded" + op_aidge_class_name
        op_aidge_constr_name = "Padded" + op_aidge_constr_name
    else:
        del avg_pool_attrs['padding_dims']

    if op_aidge_class_name in dir(aidge_core):
        avg_pooling_op = aidge_core.__getattribute__(op_aidge_constr_name)(
            kernel_dims,
            **avg_pool_attrs)
    else:
        Log.warn(f"Warning: operator {op_aidge_class_name} is not supported in Aidge. This node will be filled by a GenericOperator.")
        return None

    Log.info(f"Loaded node [\033[1m\033[3m{node_name}\033[0m] of type [\033[1m\033[3m{onnx_node.op_type}\033[0m]")
    return aidge_core.Node(avg_pooling_op, name = node_name)
