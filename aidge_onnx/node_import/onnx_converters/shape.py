"""
Copyright (c) 2023 CEA-List

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0
"""
from typing import List, Tuple

import aidge_core
import onnx

from aidge_onnx.node_import import auto_register_import
from aidge_onnx.utils import get_node_attributes

from aidge_core import Log

@auto_register_import("shape")
def import_shape(onnx_node:onnx.NodeProto, input_nodes:List[Tuple[aidge_core.Node, int]], opset: int = None) -> aidge_core.Node:
    """
    :param onnx_node: ONNX node to convert
    :type onnx_node: onnx.NodeProto
    :param input_nodes: List of pairs of Aidge Nodes which constitute the input
        of the current node and their associated output index linking to the
        current Node.
    :type input_nodes: List[aidge_core.Node, int]
    :param opset: verison of the ONNX operator, default = None.
    :type opset: int, optional
    """
    node_name = onnx_node.name if onnx_node.name else onnx_node.output[0]
    onnx_attrs = get_node_attributes(onnx_node, opset)
    shape_attrs: dict = {'start': 0,'end': -1}

    # if opset < 15:
        #### Attributes
        #### Inputs
        #  data (non-differentiable) : T
        #    An input tensor.
        #### Outputs
        #  size (non-differentiable) : T1
        #    Total number of elements of the input tensor
    if opset >= 15:
        #### Attributes
        #  end (Optional) : int
        #    Negative value means counting dimensions from the back. If omitted, sizes of all axes upto (including) the last one will be included.</dd>
        #  start (Optional) : int (default is 0)
        #    Negative value means counting dimensions from the back.
        #### Inputs
        #  data (non-differentiable) : T
        #    An input tensor.
        #### Outputs
        #  shape (non-differentiable) : T1
        #    Shape of the input tensor

        if 'start' in onnx_attrs:
            shape_attrs['start'] = onnx_attrs['start']
            del onnx_attrs['start']

        if 'end' in onnx_attrs:
            shape_attrs['end'] = onnx_attrs['end']
            del onnx_attrs['end']

    if len(onnx_attrs) > 0:
        Log.warn(f"Warning: unsupported attribute(s): {onnx_attrs.keys()} for operator 'Shape' with opset {opset}.\nThis node will be filled by a GenericOperator.")
        return None

    my_node = aidge_core.Shape(**shape_attrs, name = onnx_node.output[0])
    Log.info(f"Loaded node [\033[1m\033[3m{node_name}\033[0m] of type [\033[1m\033[3m{onnx_node.op_type}\033[0m]")
    return my_node
